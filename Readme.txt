- In "Player Settings" -> "Other Settings" -> "Rendering": ENABLE "Multithread Rendering" - otherwise NatCam does not behave well on Android 
- In "Player Settings" -> "Other Settings" -> "Identification": Set "Minimum API Level" to 'Android 4.3 (API level 18)' - as NatCam Professional requires at least API level 18
- The Image reference file:
	+ must be a "Texture Type": "Default"
	+ must have Read/Write permission under Unity: In "(select the file)" -> "inspector" -> "Advanced" -> check "Read/Write Enabled"
	+ watch out for the "Non power of 2" option, it should be "None": by default Unity compresses its textures by changing their width and height to match a power of 2, thus stretching or shortening the image, this often results in poor detection performances