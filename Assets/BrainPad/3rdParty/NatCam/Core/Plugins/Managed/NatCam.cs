/* 
*   NatCam Core
*   Copyright (c) 2016 Yusuf Olokoba
*/

namespace NatCamU.Core {

    using UnityEngine;
    using Platforms;
    using Dispatch;

    [CoreDoc(@"NatCam")]
    public static partial class NatCam {

        #region --Events--
        
        /// <summary>
        /// Event fired when the preview starts
        /// </summary>
        [CoreDoc(@"OnStart")]
        public static event PreviewCallback OnStart {
            add { Implementation.OnStart += value; }
            remove { Implementation.OnStart -= value; }
        }
        /// <summary>
        /// Event fired on each camera preview frame
        /// </summary>
        [CoreDoc(@"OnFrame")]
        public static event PreviewCallback OnFrame {
            add { Implementation.OnFrame += value; }
            remove { Implementation.OnFrame -= value; }
        }
        #endregion


        #region --Properties--

        /// <summary>
        /// The backing implementation NatCam uses on this platform
        /// </summary>
        [CoreDoc(@"Implementation")]
        public static readonly INatCam Implementation;
        /// <summary>
        /// The camera preview as a Texture
        /// </summary>
        [CoreDoc(@"Preview")]
        public static Texture Preview { get { return Implementation.Preview; }}
        /// <summary>
        /// Get or set the active camera.
        /// </summary>
        [CoreDoc(@"Camera")]
        public static DeviceCamera Camera {
            get { return Implementation.Camera; }
            set {
                #if NATCAM_PRO || NATCAM_PROFESSIONAL
                if (IsRecording) {
                    Debug.LogError("NatCam Error: Cannot switch cameras while recording");
                    return;
                }
                #endif
                if (value != -1) Implementation.Camera = value;
            }
        }
        /// <summary>
        /// Is the preview running?
        /// </summary>
        [CoreDoc(@"IsPlaying")]
        public static bool IsPlaying { get { return Implementation.IsPlaying; }}
        /// <summary>
        /// Enable or disbale verbose logging
        /// </summary>
        public static bool Verbose { set { Implementation.Verbose = value; }}
        #endregion


        #region --Operations--

        /// <summary>
        /// Start the camera preview
        /// </summary>
        /// <param name="camera">Optional. Camera that the preview should start from</param>
        [CoreDoc(@"Play")]
        public static void Play (DeviceCamera camera = null) {
            if (camera) Implementation.Camera = camera;
            if (Implementation.IsInitialized) Implementation.Resume();
            else Implementation.Play();
        }

        /// <summary>
        /// Pause the camera preview
        /// </summary>
        [CoreDoc(@"Pause")]
        public static void Pause () {
            Implementation.Pause();
        }

        /// <summary>
        /// Release all NatCam resources
        /// </summary>
        [CoreDoc(@"Release")]
        public static void Release () {
            Implementation.Release();
        }

        /// <summary>
        /// Capture a photo
        /// </summary>
        /// <param name="callback">The callback to be invoked when NatCam receives the captured photo</param>
        [CoreDoc(@"CapturePhoto", @"CapturePhotoDiscussion"), Code(@"TakeAPhoto")]
        public static void CapturePhoto (PhotoCallback callback) {
            if (callback == null) {
                Debug.LogError("NatCam Error: Cannot capture photo when callback is null");
                return;
            }
            if (!Implementation.IsPlaying) {
                Debug.LogError("NatCam Error: Cannot capture photo when session is not running");
                return;
            }
            Implementation.CapturePhoto(callback);
        }
        #endregion


        #region --Initialization--

        static NatCam () {
            // Instantiate implementation for this platform
            Implementation = 
            #if UNITY_EDITOR || UNITY_STANDALONE
            new NatCamLegacy();
            #elif UNITY_IOS
            new NatCamiOS();
            #elif UNITY_ANDROID
            new NatCamAndroid();
            #else
            new NatCamLegacy();
            #endif
            // Quit when app dies
            DispatchUtility.onQuit += Release;            
        }
        #endregion
    }
}