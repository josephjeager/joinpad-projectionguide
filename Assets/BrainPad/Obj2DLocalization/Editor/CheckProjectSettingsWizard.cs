﻿using BrainPad.Editor;
using UnityEditor;

namespace BrainPad.Obj2DLocalization.Editor
{
    public class CheckProjectSettingsWizard : ScriptableWizard
    {
        [MenuItem("BrainPad/Check Project Settings")]
        public static void CreateWizard()
        {
            if (Utils.CheckConfiguration())
            {
                EditorUtility.DisplayDialog("Project settings checked", "Your project is correctly setup to run BrainPad", "Ok");
            }
            else
            {
                EditorUtility.DisplayDialog("Project settings checked", "Some settings are not compatible with BrainPad, please run Check Project Settings again", "Ok");
            }
        }
    }
}