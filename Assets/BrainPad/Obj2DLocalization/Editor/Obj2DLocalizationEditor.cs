﻿using System.IO;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace BrainPad.Obj2DLocalization.Editor
{
    [CustomEditor(typeof(Obj2DLocalization))]
    public class Obj2DLocalizationEditor : UnityEditor.Editor
    {
        private ReorderableList list;

        private void OnEnable()
        {
            list = new ReorderableList(serializedObject, serializedObject.FindProperty("ArObjects"), true, true, true, true);
            list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                var element = list.serializedProperty.GetArrayElementAtIndex(index);
                rect.y += 2;
//			GUI.enabled = false;
//			EditorGUI.PropertyField(new Rect(rect.x, rect.y, 60, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("ArGameObjectRoot"), new  GUIContent("dsa"));
//			EditorGUI.PropertyField(new Rect(rect.x + 60, rect.y, rect.width - 60 - 30, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("ReferenceImage"), GUIContent.none);
//			EditorGUI.PropertyField(new Rect(rect.x + rect.width - 30, rect.y, 30, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("ReferenceImageWidth"), GUIContent.none);
//			EditorGUI.PropertyField(new Rect(rect.x + rect.width - 30, rect.y, 30, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("ReferenceImageWidth"), GUIContent.none);
//			GUI.enabled = true;
//			EditorStyles.label.wordWrap = true;
                EditorGUI.LabelField(new Rect(rect.x, rect.y, 60, EditorGUIUtility.singleLineHeight), "W: " + element.FindPropertyRelative("ReferenceImageWidth").floatValue);
                EditorGUI.LabelField(new Rect(rect.x + 60, rect.y, 120, EditorGUIUtility.singleLineHeight), "H: " + element.FindPropertyRelative("ReferenceImageHeight").floatValue);

                GUI.enabled = false;
                EditorGUI.PropertyField(new Rect(rect.x + 120, rect.y, rect.width - 120, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("ReferenceImage"), GUIContent.none);
                GUI.enabled = true;
            };
            list.drawHeaderCallback = (Rect rect) => { EditorGUI.LabelField(rect, "Image Targets"); };
            list.onSelectCallback = (ReorderableList l) =>
            {
                var prefab = l.serializedProperty.GetArrayElementAtIndex(l.index).FindPropertyRelative("ArGameObjectRoot").objectReferenceValue as GameObject;
                if (prefab) EditorGUIUtility.PingObject(prefab.gameObject);
            };
            list.onCanRemoveCallback = (ReorderableList l) => { return l.count > 0; };
            list.onRemoveCallback = (ReorderableList l) =>
            {
                if (EditorUtility.DisplayDialog("Remove image target", "Do you want to remove the image target and all of its generated assets?", "Yes", "No"))
                {
                    GameObject arGameObject = l.serializedProperty.GetArrayElementAtIndex(l.index).FindPropertyRelative("ArGameObjectRoot").objectReferenceValue as GameObject;
                    string referenceImageName = l.serializedProperty.GetArrayElementAtIndex(l.index).FindPropertyRelative("ReferenceImageName").stringValue;

                    DestroyImmediate(arGameObject);
                    FileUtil.DeleteFileOrDirectory("Assets/BrainPad/Generated/Textures/" + referenceImageName);
                    FileUtil.DeleteFileOrDirectory("Assets/BrainPad/Generated/Textures/" + referenceImageName + ".meta");
                    FileUtil.DeleteFileOrDirectory("Assets/BrainPad/Generated/Materials/" + Path.GetFileNameWithoutExtension(referenceImageName) + ".mtl.meta");
                    FileUtil.DeleteFileOrDirectory("Assets/BrainPad/Generated/Materials/" + Path.GetFileNameWithoutExtension(referenceImageName) + ".mtl.meta");
                    FileUtil.DeleteFileOrDirectory("Assets/BrainPad/Generated/Meshes/" + Path.GetFileNameWithoutExtension(referenceImageName) + ".asset.meta");
                    FileUtil.DeleteFileOrDirectory("Assets/BrainPad/Generated/Meshes/" + Path.GetFileNameWithoutExtension(referenceImageName) + ".asset.meta");
                    ReorderableList.defaultBehaviours.DoRemoveButton(l);
                    AssetDatabase.Refresh();
                }
            };
            list.onAddCallback = (ReorderableList l) => { Obj2DLocalizationNewImageTargetWizard.CreateWizard(); };
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, "m_Script");
            list.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }
    }
}